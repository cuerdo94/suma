/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services.suma;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")
public class SumaRecurso {
    
    @GET
    @Path("/suma")
    public String getSumaQuery(@QueryParam("Numeros")String numeros){
        String ListaNumero[] = numeros.split(",");
        Integer suma=0;
        for (int i = 0; i < ListaNumero.length; i++) {
            suma=suma+ Integer.parseInt(ListaNumero[i]);
            
        }
    
    return "El resutado de la suma: "+suma;
    }
    
}
